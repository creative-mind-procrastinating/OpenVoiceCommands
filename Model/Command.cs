﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinAI.Model
{
    class Command
    {
        public string Name { get; set; }
        public bool EqualsEnabled { get; set; }
        public string Trigger { get; set; }
        public string Respond { get; set; }
        public string CommandPath { get; set; }
    }
}
