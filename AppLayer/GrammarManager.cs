﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;

namespace WinAI.AppLayer
{
    class GrammarManager
    {
        private Choices sentences;
        public GrammarBuilder GrammarBuilder { get; set; }
        public Grammar Grammar { get; set; }

        public GrammarManager(string [] triggers, CultureInfo info)
        {
            sentences = new Choices();
            sentences.Add(triggers);

            GrammarBuilder = new GrammarBuilder(sentences);
            GrammarBuilder.Culture = info;
            Grammar = new Grammar(GrammarBuilder);
            Grammar.Enabled = true;
        }

        public void SetCultureInfo(CultureInfo info)
        {
            GrammarBuilder.Culture = info;
        }
    }
}
