﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using WinAI.Model;

namespace WinAI.AppLayer
{
    class SpeechEngine
    {
        private IReadOnlyCollection<RecognizerInfo> speechRecognizers = SpeechRecognitionEngine.InstalledRecognizers();
        private SpeechRecognitionEngine speechRecognizer;
        private SpeechSynthesizer speechSynthesis;
        CommandManager commandManager;
        GrammarManager grammarManager;

        public List<Command> Commands { get; set; }

        public SpeechEngine()
        {
            commandManager = new CommandManager();
            speechSynthesis = new SpeechSynthesizer(); 

            foreach (RecognizerInfo item in speechRecognizers)
            {
                if (item.Id.Equals("MS-1033-80-DESK"))
                {
                    grammarManager = new GrammarManager(commandManager.Triggers, item.Culture);
                    speechRecognizer = new SpeechRecognitionEngine(item.Culture);
                    speechRecognizer.SpeechRecognized += Sr_SpeechRecognized;
                    break;
                }
            }


            speechRecognizer.LoadGrammar(grammarManager.Grammar);
            speechRecognizer.SetInputToDefaultAudioDevice();
            //speechRecognizer.BabbleTimeout = new TimeSpan(0, 0, 10);
            speechRecognizer.RecognizeAsync(RecognizeMode.Multiple);

        }

        private void Sr_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string result = e.Result.Text.ToLower();
            Console.WriteLine(result);

            foreach (var item in commandManager.Commands)
            {
                if (item.EqualsEnabled)
                {
                    if (item.Trigger.Equals(result))
                    {
                        speechSynthesis.SpeakAsync(item.Respond);
                        Process.Start(CommandManager.autoITPath, item.CommandPath);
                        Console.WriteLine(item.Trigger);
                        Console.WriteLine(item.CommandPath);
                        break;
                    }
                }
                else
                {
                    if (item.Trigger.Contains(result))
                    {
                        speechSynthesis.SpeakAsync(item.Respond);
                        Process.Start(CommandManager.autoITPath, item.CommandPath);
                        Console.WriteLine(item.Trigger);
                        Console.WriteLine(item.CommandPath);
                        break;
                    }
                }
            }          
        }
    }
}
