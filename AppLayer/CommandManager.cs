﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinAI.Model;

namespace WinAI.AppLayer
{
    class CommandManager
    {
        private static string commandsPath = AppDomain.CurrentDomain.BaseDirectory;
        private static string commandsJsonPath = commandsPath + "Commands.json";
        private static string scriptsPath = commandsPath + "Scripts\\";
        public static string autoITPath = commandsPath + "AutoIt\\AutoIt3_x64.exe";

        public string[] Triggers { get; set; }
        public List<Command> Commands { get; set; }

        public CommandManager()
        {
            Commands = LoadCommands();
        }

        private List<Command> LoadCommands()
        {
            string commandsJson = File.ReadAllText(commandsJsonPath);
            var commands = JsonConvert.DeserializeObject<List<Command>>(commandsJson);
            Triggers = new string[commands.Count];

            for (int i = 0; i < commands.Count; i++)
            {
                commands[i].CommandPath = scriptsPath + commands[i].CommandPath;
                Triggers[i] = commands[i].Trigger;
            }

            return commands;
        }
    }
}
