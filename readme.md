# Synopsis
Free and open voice commands for Elite: Dangerous based on AutoIt script software.
# Code Example
TODO
# Motivation
Free and simple voice commands is missing on the internet!
# Installation
1. Install Microsoft Speech Recognizer 8.0 for Windows (English - US).
Help at: https://zulja.wordpress.com/2015/08/11/installing-additional-language-packs-for-windows-10-speech-recognition/
	* Select the Start button, then Settings > Time & language > Region & language. 
	* Select Add a language.
	* Add English (United States)
	* Wait until installation is completed.
	* Select installed language and then select Options
	* Download and install Speech.	
2. Run app.

# API Reference
[Scripts](https://gitlab.com/creative-mind-procrastinating/OpenVoiceCommands/tree/master/Scripts)
[JSON command api](https://gitlab.com/creative-mind-procrastinating/OpenVoiceCommands/blob/master/Commands.json)
[AutoIt documentation](https://www.autoitscript.com/autoit3/docs/functions/Send.htm)
# Tests
TODO
# Contributors
TODO
# License
TODO